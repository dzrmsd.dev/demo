package ramos.diaz.demo.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import ramos.diaz.demo.web.repository.PetRepository;
import ramos.diaz.demo.web.entities.Pet;

@Controller
public class PetsController {
	
	@Autowired	
	private PetRepository petRepository;
	
	@RequestMapping("/pets")
	public String index(Model model) {
		List<Pet> listPets = petRepository.findAll();
	    model.addAttribute("listPets", listPets);;
		return "pets/index";
	}
	
	@RequestMapping("/pets/new")
	public String add(Model model) {
		Pet pet = new Pet();
		model.addAttribute("pet", pet);
		return "pets/new";
	}
	
	@RequestMapping(value = "/pets/save", method = RequestMethod.POST)
	public String save(@ModelAttribute("pet") Pet pet) {
		petRepository.save(pet);
	    return "redirect:/pets";
	}
	
	@RequestMapping("/pets/edit/{id}")
	public ModelAndView showEditProductPage(@PathVariable(name = "id") int id) {
	    ModelAndView model = new ModelAndView("pets/edit");
	    Pet pet = petRepository.getOne(id);
	    model.addObject("pet", pet);
	    return model;
	}
	
	@RequestMapping("/pets/delete/{id}")
	public String deleteProduct(@PathVariable(name = "id") int id) {
	    petRepository.deleteById(id);
	    return "redirect:/pets";
	}

}
