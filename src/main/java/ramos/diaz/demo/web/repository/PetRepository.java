package ramos.diaz.demo.web.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import ramos.diaz.demo.web.entities.Pet;


public interface PetRepository extends JpaRepository<Pet, Integer>{
	
}