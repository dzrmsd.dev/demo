package ramos.diaz.demo.web.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ramos.diaz.demo.web.entities.Pet;

@Service
@Transactional
public class PetService {
	
	@Autowired
	private PetService petService;

	public List<Pet> findAll(){
		List<Pet> pets = petService.findAll();
		return pets;
	}
}
